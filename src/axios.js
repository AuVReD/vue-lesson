const axios = require('axios').default;
import store from './store/index'


let baseURL
let backend_ip = 'localhost'
let backend_port = '5000'

baseURL = 'http://' + backend_ip + ':' + backend_port + '/api'

axios.defaults.baseURL = baseURL;

axios.interceptors.request.use(
    config => {
        store.dispatch('startLoading')
        return config;
    },
    error => {
        store.dispatch('stopLoading')
        return response;
    }
);

axios.interceptors.response.use(
    response => {
        store.dispatch('stopLoading')
        return response;
    },
    error => {
        store.dispatch('stopLoading')
        return response;
    }
);

export default axios

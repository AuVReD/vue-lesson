import Vue from 'vue'
import Router from 'vue-router'
const Welcome = () => import('./components/Welcome')
const ThemesPage = () => import('./components/themes/ThemesPage')
const ActionChoice = () => import('./components/action_choice/ActionChoice')
const Exam = () => import('./components/exam/Exam')
const ExamEnd = () => import('./components/exam/ExamEnd')
const ShowAnswers = () => import('./components/show_answers/ShowAnswers')
const TestWithAnswers = () => import('./components/test_with_answers/TestWithAnswers')

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Welcome
        },
        {
            path: '/themes',
            component: ThemesPage
        },
        {
            path: '/action_choice',
            component: ActionChoice
        },
        {
            path: '/exam',
            component: Exam
        },
        {
            path: '/exam_end',
            component: ExamEnd
        },
        {
            path: '/show_answers',
            component: ShowAnswers
        },
        {
            path: '/test_with_answers',
            component: TestWithAnswers
        }
    ]
})